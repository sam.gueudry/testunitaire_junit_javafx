package sam.testunit.testunitairecours;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import sam.testunit.testunitairecours.testCase.TestJSONFile;
import sam.testunit.testunitairecours.testCase.TestSignUpController;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   TestJSONFile.class,
   TestSignUpController.class
})

public class TestSuite {}
