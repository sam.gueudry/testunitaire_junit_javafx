package sam.testunit.testunitairecours.testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import sam.testunit.testunitairecours.model.JsonModelApi;
import sam.testunit.testunitairecours.model.User;
import sam.testunit.testunitairecours.services.UserService;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class TestSignUpController {
  //Reset JSON File for Users Test

  @Test
  public void userIsCreate() {
    UserService us = new UserService();
    try {
      us.createUser('A', "my name is", "premin", "Nom.prenom@google.com", "0650120920", "9 rue de lre","mdp");
      assertNotNull(us.getUserByMail("Nom.prenom@google.com"));
    } catch (Exception e) {
      // fail(e.getMessage());
    }
  }

  @Test
  public void canGetUserByMail() throws Exception {
    UserService us = new UserService();
    System.out.println(us.getAllUsers().size());
    User user = us.getUserByMail("Nom.prenom@google.com");
    assertTrue(user.getMail().equals("Nom.prenom@google.com"));
  }

  @Test
  public void UsersCollectionLoadingInJson() {
    // Ici on est censé trouver notre User nom.prenom@g.com
    UserService us = new UserService();
    us.loadData();
    System.out.println(us.getAllUsers().size());
  }

  @Test
  public void UserCantHaveSameEmailAd() {
    UserService us = new UserService();
    try {
      us.createUser('A', "Nom", "Prenom", "Nom.prenom@g.com", "0650120920", "9 rue de lre","mdp");
      us.createUser('A', "Nom", "Prenom", "Nom.prenom@g.com", "0650120920", "9 rue de lre","mdp");
      fail("Impossible un User à été créer avec une ad mail identique à a autre user");
    } catch (Exception e) {
      System.out.println(e);
      assertTrue(true);
    }
  }

  @Test
  public void EachFieldNotEmpty() {
    UserService us = new UserService();
    boolean state = true;
    for (int i = 0; i < 6; i++) {
      switch (i) {
        case 0:
          try {
            us.createUser(' ', "GUDU", "Pren", "mail@zae.fr", "0949390", "ADRESSSE","mdp");
            state = false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
        case 1:
          try {
            us.createUser('U', "", "Pren", "mail@zae.fr", "0949390", "ADRESSSE","mdp");
            state = false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
        case 2:
          try {
            us.createUser('U', "GUDU", "", "mail@zae.fr", "0949390", "ADRESSSE","mdp");
            state = false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
        case 3:
          try {
            us.createUser('U', "GUDU", "Pren", "", "0949390", "ADRESSSE","mdp");
            state = false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
        case 4:
          try {
            us.createUser('U', "GUDU", "Pren", "mail@zae.fr", "", "ADRESSSE","mdp");
            state = false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
        case 5:
          try {
            us.createUser('U', "GUDU", "Pren", "mail@zae.fr", "0949390", "","mdp");
            state = false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
      }
    }
    assertTrue(state);
  }

  @Test
  public void invalidEmailFormat() {
    UserService us = new UserService();
    boolean state = true;
    for (int i = 0; i < 5; i++) {
      switch (i) {
        case 0:
          try {
            us.createUser('A', "Namo", "Le poissson", "CeciEstUnFaucMail", "095093039", "adresse","mdp");
            state=false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
      case 1:
          try {
            us.createUser('A', "Namo", "Le poissson", "CeciEstUn@FaucMail", "095093039", "adresse","mdp");
            state=false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
      case 2:
          try {
            us.createUser('A', "Namo", "Le poissson", "@FaucMail", "095093039", "adresse","mdp");
            state=false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
      case 3:
          try {
            us.createUser('A', "Namo", "Le poissson", "TEs@.fr", "095093039", "adresse","mdp");
            state=false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
      case 4:
          try {
            us.createUser('A', "Namo", "Le poissson", "te@ezr. ", "095093039", "adresse","mdp");
            state=false;
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
      }
    }
    assertTrue(state);
  }
}
