package sam.testunit.testunitairecours.testCase;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import sam.testunit.testunitairecours.controller.LoginController;
import sam.testunit.testunitairecours.model.User;
import sam.testunit.testunitairecours.services.UserService;

public class TestLoginController {

  @Test
  public void TestLogin() {
    LoginController lc = new LoginController();  
    UserService us = new UserService();
    try {
      us.createUser('A', "my name is", "premin", "s.g@google.com", "0650120920", "9 rue de lre","mdpTest");
      User user = lc.login("s.g@google.com", "mdpTest");
      assertNotNull(user);
      User userBlank = lc.login("", "");
      assertNull(userBlank);
      User userIdBlank = lc.login("", "mdpTest");
      assertNull(userIdBlank);
      User usermdpBlank = lc.login("Nom.prenom@google.com", "");
      assertNull(usermdpBlank);
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }
}
