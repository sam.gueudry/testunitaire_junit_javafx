package sam.testunit.testunitairecours.helpers;

public class Helpers {
  private Helpers() {
  }
  public static Exception customException(String message) {
    return new Exception(message);
  }
}
