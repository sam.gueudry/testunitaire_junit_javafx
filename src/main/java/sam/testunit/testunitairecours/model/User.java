package sam.testunit.testunitairecours.model;

public class User {
  private Long id;
  private String mdp;
  private char role;
  private String nom;
  private String prenom;
  private String mail;
  private String telephone;
  private String adresse;

  public User() {
  }

  public User(Long id,String mdp, char role, String nom, String prenom,
   String mail, String telephone, String adresse) {
    this.id = id;
    this.mdp = mdp;
    this.role = role;
    this.nom = nom;
    this.prenom = prenom;
    this.mail = mail;
    this.telephone = telephone;
    this.adresse = adresse;
  }

  public long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getMdp() {
    return this.mdp;
  }

  public void setMdp(String mdp) {
    this.mdp = mdp;
  }

  public char getRole() {
    return this.role;
  }

  public void setRole(char role) {
    this.role = role;
  }

  public String getNom() {
    return this.nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return this.prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getMail() {
    return this.mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getTelephone() {
    return this.telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getAdresse() {
    return this.adresse;
  }

  public void setAdresse(String adresse) {
    this.adresse = adresse;
  }

  @Override
  public String toString() {
    return "{" +
      " id='" + getId() + "'" +
      ", role='" + getRole() + "'" +
      ", nom='" + getNom() + "'" +
      ", prenom='" + getPrenom() + "'" +
      ", mail='" + getMail() + "'" +
      ", telephone='" + getTelephone() + "'" +
      ", adresse='" + getAdresse() + "'" +
      "}";
  }

}
