package sam.testunit.testunitairecours.model;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import sam.testunit.testunitairecours.services.UserService;

public class JsonModelApi {
  private JsonModelApi() {
  }

  public static void save(JSONArray data, String fileName) {
    fileName = fileName.isEmpty() ? "Users.json": fileName;
    // Write JSON file
    try (FileWriter file = new FileWriter(fileName)) {
      // We can write any JSONArray or JSONObject instance to the file
      file.write(data.toJSONString());
      file.flush();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void getData(UserService service,String fileName) {
    JSONParser jsonParser = new JSONParser();
    fileName = fileName.isEmpty() ? "Users.json": fileName;

    try (FileReader reader = new FileReader(fileName)) {
      // Read JSON file
      Object obj = jsonParser.parse(reader);

      JSONArray collection = (JSONArray) obj;
      collection.forEach(usr -> service.addUser(parseUserObject((JSONObject) usr)));

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static User parseUserObject(JSONObject user) {
    Long id = (Long) user.get("id") ;
    String mdp = (String) user.get("mdp");
    char role = ((String) user.get("role")).charAt(0);
    String nom = (String) user.get("nom");
    String prenom = (String) user.get("prenom");
    String mail = (String) user.get("mail");
    String telephone = (String) user.get("telephone");
    String adresse = (String) user.get("adresse");
    return new User(id,mdp, role, nom, prenom, mail, telephone, adresse);
  }
}
