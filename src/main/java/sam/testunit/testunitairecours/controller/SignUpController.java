package sam.testunit.testunitairecours.controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import sam.testunit.testunitairecours.App;
import sam.testunit.testunitairecours.services.UserService;

public class SignUpController {

    private UserService userService = new UserService();

    @FXML
    private CheckBox checkboxAdmin;
    @FXML
    private CheckBox checkboxUser;
    @FXML
    private TextField txtFNom;
    @FXML
    private TextField txtFPrenom;
    @FXML
    private TextField txtFMail;
    @FXML
    private TextField txtFTelephone;
    @FXML
    private TextField txtFAdresse;
    @FXML
    private TextField txtFMdp;
    @FXML
    private TextField txtFMdpVerif;

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("login");
    }

    @FXML
    private void signUp() throws IOException {
        char role = this.checkboxAdmin.selectedProperty().get() ? 'A' : 'U';
        String nom = !txtFNom.getText().isEmpty() ? txtFNom.getText() : "";
        String prenom = !txtFPrenom.getText().isEmpty() ? txtFPrenom.getText() : "";
        String mail = !txtFMail.getText().isEmpty() ? txtFMail.getText() : "";
        String telephone = !txtFTelephone.getText().isEmpty() ? txtFTelephone.getText() : "";
        String adresse = !txtFAdresse.getText().isEmpty() ? txtFAdresse.getText() : "";
        String mdp = !txtFMdp.getText().isEmpty() ? txtFMdp.getText() : "";
        String mdpVerif = !txtFMdpVerif.getText().isEmpty() ? txtFMdpVerif.getText() : "";

        try {

            if (userService.validateChamps(role, nom, prenom, mail, telephone, adresse, mdp, mdpVerif)) {
                userService.createUser(role, nom, prenom, mail, telephone, adresse, mdp);
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("User created !");
                alert.setHeaderText("Super,");
                alert.setContentText("Vous pouvez maintenant vous connecter");

                alert.showAndWait();
                App.setRoot("login");
            } else {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Invalid field");
                alert.setHeaderText("Results:");
                alert.showAndWait();
            }
        } catch (Exception e) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Invalid field");
            alert.setHeaderText("Results:");
            alert.setContentText(e.getMessage());

            alert.showAndWait();
        }
    }

    @FXML
    private void checkboxClick(ActionEvent event) {
        switch (((CheckBox) event.getSource()).getId()) {
            case "checkboxAdmin":
                this.checkboxUser.selectedProperty().set(!this.checkboxAdmin.selectedProperty().get());
                break;
            case "checkboxUser":
                this.checkboxAdmin.selectedProperty().set(!this.checkboxUser.selectedProperty().get());
                break;
        }
        ;

    }

}