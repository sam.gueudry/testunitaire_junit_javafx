package sam.testunit.testunitairecours.controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import sam.testunit.testunitairecours.App;
import sam.testunit.testunitairecours.model.User;
import sam.testunit.testunitairecours.services.UserService;

public class DashboardControlleur {

  private UserService userService = new UserService();

  @FXML
  private TableView<User> userTable;
  @FXML
  private TableColumn<User,Integer> idCol;
  @FXML
  private TableColumn<User,String> nameCol;
  @FXML
  private TableColumn<User,String> roleCol;
  @FXML
  private TableColumn<User,String> prenomCol;
  @FXML
  private TableColumn<User,String> mailCol;
  @FXML
  private TableColumn<User,String> telephoneCol;
  @FXML
  private TableColumn<User,String> adresseCol;
  @FXML
  private TextField txtFNom;
  @FXML
  private TextField txtFPrenom;
  @FXML
  private TextField txtFMail;
  @FXML
  private TextField txtFTelephone;
  @FXML
  private TextField txtFAdresse;
  @FXML
  private TextField txtFMdp;
  @FXML
  private TextField txtFMdpVerif;

  @FXML
  private void initialize() {
    if (userService.getUserLogIn().getRole() == 'U') {
      fillUserInfo();
    }

    if (userService.getUserLogIn().getRole() == 'A') {
      fillTableData();
    }
  }

  // ######################### partie Admin

  public void fillTableData() {
    idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
    nameCol.setCellValueFactory(new PropertyValueFactory<>("nom"));
    roleCol.setCellValueFactory(new PropertyValueFactory<>("role"));
    prenomCol.setCellValueFactory(new PropertyValueFactory<>("prenom"));
    mailCol.setCellValueFactory(new PropertyValueFactory<>("mail"));
    telephoneCol.setCellValueFactory(new PropertyValueFactory<>("telephone"));
    adresseCol.setCellValueFactory(new PropertyValueFactory<>("adresse"));
    this.userTable.getItems().addAll(userService.getAllUsers());
  }

  // ################################# Partie Utilisateur

  public void fillUserInfo() {
    this.txtFNom.setPromptText(userService.getUserLogIn().getNom());
    this.txtFPrenom.setPromptText(userService.getUserLogIn().getPrenom());
    this.txtFMail.setPromptText(userService.getUserLogIn().getMail());
    this.txtFTelephone.setPromptText(userService.getUserLogIn().getTelephone());
    this.txtFAdresse.setPromptText(userService.getUserLogIn().getAdresse());
    this.txtFMdp.setPromptText(userService.getUserLogIn().getMdp());
  }

  @FXML
  public void modifierProfilAction() throws Exception {
    User currentUser = userService.getUserLogIn();
    String nom = !this.txtFNom.getText().isBlank() ? this.txtFNom.getText() : currentUser.getNom();
    String prenom = !this.txtFPrenom.getText().isBlank() ? this.txtFPrenom.getText() : currentUser.getPrenom();
    String mail = !this.txtFMail.getText().isBlank() ? this.txtFMail.getText() : currentUser.getMail();
    String telephone = !this.txtFTelephone.getText().isBlank() ? this.txtFTelephone.getText()
        : currentUser.getTelephone();
    String adresse = !this.txtFAdresse.getText().isBlank() ? this.txtFAdresse.getText() : currentUser.getAdresse();
    String mdp = !this.txtFMdp.getText().isBlank() ? this.txtFMdp.getText() : currentUser.getMdp();
    String mdpVerif = !this.txtFMdpVerif.getText().isBlank() ? this.txtFMdpVerif.getText() : currentUser.getMdp();

    currentUser.setNom(nom);
    currentUser.setPrenom(prenom);
    currentUser.setMail(mail);
    currentUser.setTelephone(telephone);
    currentUser.setAdresse(adresse);
    currentUser.setMdp(mdp);
    try {
      userService.updateUser(currentUser, mdpVerif);
      Alert alert = new Alert(AlertType.INFORMATION);
      alert.setTitle("Modification réussi");
      alert.setHeaderText("Votre profil à bien été modifié");
      alert.setContentText("");
      alert.showAndWait();
    } catch (Exception exception) {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setTitle("Impossible de modifier le profil");
      alert.setHeaderText("Veuillez vérifier les champs, il y a des erreurs");
      alert.setContentText("l'email exite surement déjà, les mdp ne sont pas identiques");
      alert.showAndWait();
    }
  }

  @FXML
  public void logoutAction() throws IOException {
    userService.userLogOut();
    App.setRoot("login");
  }
}
