package sam.testunit.testunitairecours.controller;

import java.io.IOException;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import sam.testunit.testunitairecours.App;
import sam.testunit.testunitairecours.helpers.Helpers;
import sam.testunit.testunitairecours.model.User;
import sam.testunit.testunitairecours.services.UserService;

public class LoginController {

    private UserService userService = new UserService();

    @FXML
    private TextField txtFEmail;
    @FXML
    private TextField txtFMdp;

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("signup");
    }

    @FXML
    public void loginAction() throws IOException {
        String login = !txtFEmail.getText().isEmpty() ? txtFEmail.getText() : "";
        String mdp = !txtFMdp.getText().isEmpty() ? txtFMdp.getText() : "";
        User user = login(login, mdp);
        if (user != null) {
            String dashboard = (user.getRole() == 'U') ? "user" : "admin";
            dashboard = "dashboard" + dashboard;
            App.setRoot(dashboard);
        } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Erreur de connexion");
            alert.setHeaderText("Une erreur à eu lieu ");
            alert.setContentText("Invalid Email / Mdp");
            alert.showAndWait();
        }
    }

    public User login(String login, String mdp) {
        try {
            User user = userService.getUserByMail(login);
            if (user.getMdp().equals(mdp)) {
                userService.setUserLogIn(user);
                return user;
            } else {
                throw Helpers.customException("Email / Mdp erreur");
            }
        } catch (Exception e) {
            return null;
        }
    }

    @FXML
    public void mdpForgetAction() {
        TextInputDialog dialog = new TextInputDialog("ad.email@gmail.com");
        dialog.setTitle("Mot de passe oublié");
        dialog.setHeaderText("Réinitialiser votre mdp");
        dialog.setContentText("Entrer votre email/id : ");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                User u = userService.getUserByMail(result.get());
                u.setMdp("mdp");
                userService.save();
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Modification réussi");
                alert.setHeaderText("Votre mdp à bien été modifié");
                alert.setContentText("nouveau mdp => \"mdp\"");
                alert.showAndWait();

            } catch (Exception e) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("not exist");
            }
        }

        // The Java 8 way to get the response value (with lambda expression).
        // result.ifPresent(name -> System.out.println("Your name: " + name));
    }

}
