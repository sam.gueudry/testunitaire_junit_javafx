package sam.testunit.testunitairecours.services;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import sam.testunit.testunitairecours.env;
import sam.testunit.testunitairecours.helpers.CustomException;
import sam.testunit.testunitairecours.model.JsonModelApi;
import sam.testunit.testunitairecours.model.User;

public class UserService {
  private static List<User> data = new ArrayList<>();
  private static User userLogIn = null;
  private String fileName = env.filename;

  public UserService() {
    if (data.isEmpty())
      this.loadData();
  }

  public List<User> getAllUsers() {
    return data;
  };

  public void addUser(User user) {
    data.add(user);
  }

  public User getUser(Long id) throws CustomException {
    for (User g : data) {
      if (g.getId() == id)
        return g;
    }
    throw new CustomException("Invalid User id");
  }

  public User createUser(char role, String nom, String prenom, String mail, String telephone, String adresse,
      String mdp)
      throws CustomException {
    if (!this.validateChamps(role, nom, prenom, mail, telephone, adresse, mdp, mdp))
      throw new CustomException("Invalid data format, smthg blank or email format invalid");

    for (User g : data) {
      if (g.getMail().toLowerCase().equals(mail.toLowerCase())) {
        // System.out.println("ERRRRRRRERU");
        throw new CustomException("Invalid User mail, already exists");
      }
    }
    System.out.println("Size");
    // System.out.println(data.size());
    Long id = !data.isEmpty() ? data.get(data.size() - 1).getId() + 1 : 1;
    User user = new User(id, mdp, role, nom, prenom, mail, telephone, adresse);
    // System.out.println(user.getMail());
    data.add(user);
    this.save();
    return user;
  }

  public User createUser(char role, String nom, String prenom, String mail, String telephone, String adresse,
      String mdp, String mdpVerify)
      throws CustomException {
    if (!this.validateChamps(role, nom, prenom, mail, telephone, adresse, mdp, mdpVerify))
      throw new CustomException("Invalid data format, smthg blank or email format invalid");
    return createUser(role, nom, prenom, mail, telephone, adresse, mdp);
  }

  public boolean validateChamps(char role, String nom, String prenom, String mail, String telephone, String adresse,
      String mdp, String mdpVerify) {
    boolean state = true;
    String regex = "^[\\w!#$%&’+/=?`{|}~^-]+(?:\\.[\\w!#$%&’+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    Pattern pattern = Pattern.compile(regex);

    while (state) {
      state = (role == 'A' || role == 'U');
      if (!state)
        break;
      state = !nom.isBlank();
      if (!state)
        break;
      state = !prenom.isBlank();
      if (!state)
        break;
      Matcher matcher = pattern.matcher(mail);
      state = matcher.matches();
      if (!state)
        break;
      state = !telephone.isBlank();
      if (!state)
        break;
      state = !adresse.isBlank();
      if (!state)
        break;
      state = !mdp.isBlank();
      if (!state)
        break;
      state = mdp.equals(mdpVerify);
      if (!state)
        break;
      break;
    }
    return state;
  }

  public User updateUser(Long id, char role, String nom, String prenom, String mail, String telephone, String adresse,
      String mdp, String mdpVerif)
      throws CustomException {

    if (!this.validateChamps(role, nom, prenom, mail, telephone, adresse, mdp, mdpVerif))
      throw new CustomException("Invalid data format, smthg blank or email format invalid");

    for (User g : data) {
      if (g.getId() != id) {
        if (g.getMail().toLowerCase().equals(mail.toLowerCase())) {
          // System.out.println("ERRRRRRRERU");
          throw new CustomException("Invalid User mail, already exists");
        }
      }
    }

    for (User g : data) {
      if (g.getId() == id) {
        g.setRole(role);
        g.setNom(nom);
        g.setPrenom(prenom);
        g.setMail(mail);
        g.setTelephone(telephone);
        g.setAdresse(adresse);
      }
      this.save();
      return g;
    }
    throw new CustomException("Invalid User id");
  }

  public User updateUser(User user, String mdpVerif) throws CustomException {
    try {
      return updateUser(user.getId(), user.getRole(), user.getNom(), user.getPrenom(), user.getMail(),
          user.getTelephone(), user.getAdresse(), user.getMdp(), mdpVerif);
    } catch (Exception e) {
      throw new CustomException("Une erreur à eu lieu");
    }
  }

  // public Response deleteUser(Long id) throws CustomException {
  // for(User g : data) {
  // if (g.getId() == id) {
  // data.remove(g);
  // return new Response(String.format("User %d deleted", id));
  // }
  // }
  // throw new CustomException("Invalid User id");
  // }

  public User getUserByMail(String mail) throws CustomException {
    for (User u : data) {
      // System.out.println("USER: ");
      // System.out.println(u.getMail());
      // System.out.println(mail);
      if (u.getMail().toLowerCase().equals(mail.toLowerCase())) {
        // System.out.println("YAAA");
        return u;
      }
    }
    throw new CustomException("User with this mail dosn't exist");
  }

  public void save() {
    JSONArray userList = new JSONArray();
    for (User user : data) {
      JSONObject UserDetails = new JSONObject();
      UserDetails.put("id", user.getId());
      UserDetails.put("mdp", user.getMdp());
      UserDetails.put("role", "" + user.getRole());
      UserDetails.put("nom", user.getNom());
      UserDetails.put("prenom", user.getPrenom());
      UserDetails.put("mail", user.getMail());
      UserDetails.put("telephone", user.getTelephone());
      UserDetails.put("adresse", user.getAdresse());

      userList.add(UserDetails);
    }
    JsonModelApi.save(userList, this.fileName);
  }

  public void loadData() {
    JsonModelApi.getData(this, this.fileName);
  }

  public void setUserLogIn(User u) {
    userLogIn = u;
  }

  public User getUserLogIn() {
    return userLogIn;
  }

  public void userLogOut() {
    userLogIn = null;
  }
}
